ready(".hs_cos_wrapper_type_form form", function (element) {
  bfi_init(element);
  oneFieldForm(element);
  // select(element);
});

function oneFieldForm(form) {
  if ($(form).find("> .hs-form-field").length === 1) {
    $(form).addClass("oneFieldForm");
  }
}

function select(form) {
  const $form = $(form);
  if ($form.hasClass("new-select-custom")) {
    return;
  }
  $form.addClass("new-select-custom");
  $form.find("select").each(function () {
    var parent = $(this).parent(),
      options = $(this).find("option"),
      activeOption = $(this).find("option[value=" + $(this).val() + "]"),
      header = activeOption.text() || "Please select";

    parent.addClass("dropdown_select");
    parent.append(
      '<div class="dropdown-header"><span>' + header + "</span></div>"
    );
    parent.append('<ul class="dropdown-list" style="display: none;"></ul>');
    options.not(":disabled").each(function () {
      if ($(this).text() != "") {
        parent
          .find("ul.dropdown-list")
          .append(
            '<li data-value="' +
              $(this).val() +
              '"' +
              ($(this).val() === activeOption.val() ? "class='selected'" : "") +
              ">" +
              $(this).text() +
              "</li>"
          );
      }
    });
  });

  $form.find(".dropdown-header").click(function (event, element) {
    if (!$(this).hasClass("slide-down")) {
      $(this).addClass("slide-down");
      $(this)
        .siblings(".dropdown-list")
        .slideDown(function () {
          $(this).addClass("list-down");
        });
    } else {
      $(this).removeClass("slide-down");
      $(this).siblings(".dropdown-list").removeClass("list-down");
      $(this).siblings(".dropdown-list").slideUp();
    }
    $(this).children(".arrow-white").toggle();
  });

  $form.find(".dropdown-list li").click(function () {
    var choose = $(this).text(),
      value = $(this).attr("data-value");

    $(this).parents(".dropdown-list").removeClass("list-down");
    $(this).parent().siblings(".dropdown-header").find("span").text(choose);
    $(this)
      .parent()
      .siblings(".dropdown-label")
      .addClass("dropdown-label--focus");
    $(this).parent().siblings("select").find("option").removeAttr("selected");
    $(this)
      .parent()
      .siblings("select")
      .val(value)
      .find('option[value="' + value + '"]')
      .attr("selected", "selected")
      .change();
    $(this).parent().find("li").removeClass("selected");
    $(this).addClass("selected");
    $(this)
      .parent()
      .siblings(".dropdown-header")
      .toggleClass("slide-down")
      .siblings(".dropdown-list")
      .slideUp();
    $(this)
      .parent()
      .siblings(".dropdown-header")
      .children(".arrow-white")
      .toggle();
  });
  $form.find(".dropdown_select .input").click(function (event) {
    event.stopPropagation();
  });
}

function smartSelect(elem) {
  var dynamicDropdown = $(elem)
    .parents(".hs-form-field")
    .siblings(".hs-form-field");

  if (dynamicDropdown.hasClass("rendered") == false) {
    var parent2 = dynamicDropdown.find("select").parent();

    if (!parent2.hasClass("dropdown_select")) {
      hideEmptyLabel();
      dynamicDropdown.addClass("rendered");

      parent2
        .addClass("dropdown_select")
        .append('<div class="dropdown-header"><span></span></div>')
        .append('<ul class="dropdown-list" style="display: none;"></ul>')
        .find(".dropdown-header span")
        .html(dynamicDropdown.find("option:first").text());

      dynamicDropdown.find("option").each(function () {
        if ($(this).val() != "")
          parent2
            .find("ul.dropdown-list")
            .append(
              '<li data-value="' +
                $(this).val() +
                '">' +
                $(this).val() +
                "</li>"
            );
      });

      dynamicDropdown.find(".dropdown-header").click(function () {
        $(this)
          .toggleClass("slide-down")
          .siblings(".dropdown-list")
          .slideToggle();
        $(this).children(".arrow-white").toggle();
      });

      dynamicDropdown.find(".dropdown-list li").click(function () {
        var choose = $(this).text(),
          value = $(this).attr("data-value"),
          dh = $(this).parents(".dropdown_select");

        $(this).parents(".dropdown-list").removeClass("list-down");
        dh.find(".dropdown-header span").text(choose);
        dh.find("select").find("option").removeAttr("selected");
        dh.find("select").val(value).change();
        dh.find('option[value="' + value + '"]').attr("selected", "selected");
        $(this).parent().find("li").removeClass("selected");
        $(this).addClass("selected");
        dh.find(".dropdown-header")
          .toggleClass("slide-down")
          .siblings(".dropdown-list")
          .slideToggle();
        dh.find(".dropdown-header").children(".arrow-white").toggle();
      });
      dynamicDropdown.find(".dropdown_select .input").click(function (event) {
        event.stopPropagation();
      });
    }
  }
}
