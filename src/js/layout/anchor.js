$('.co-cta-anchor').on('click', function (e) {
  if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
    var target = $(this.hash);
    var additionalOffest = 0;
    target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
    if (target.length) {

      if ($('.co-header').length) {
        additionalOffest = $('.co-header').height();
      }

      $('html,body').animate({
        scrollTop: $(this.hash).offset().top - additionalOffest
      }, 500);
      return false;
    }
  }
});