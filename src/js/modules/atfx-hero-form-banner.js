function formListener(formId, hubdbTable, hubId) {
    window.addEventListener('message', () => {
        //  look only for the form from this module
        if (event.data.id === formId) {
            const formId = event.data.id;
            const countrySelect = $(`[id*=${ formId }] select[name=country_of_residence]`);

            //  wait untill form is ready
            if (event.data.type === 'hsFormCallback' && event.data.eventName === 'onFormReady') {

              //  handle changing select value
              $(countrySelect).on('change', function() {
                handlePreventSubmit(formId, $(this).val(), hubdbTable, hubId);
              });
          
              //  change select value of country field to the country detected with SMART rules
              //  in "atfx-country-detection" global module - "detectedCountry" JS variable.
              if (detectedCountry) {
                 //  if detectedCountry is true, trigger "change" event on hidden select option
                $(countrySelect).val(detectedCountry).change();    
              } else {
                //  otherwise, disable submit button
                $(`[id*=${ formId }] .hs_submit`).addClass('hs_submit--disabled');
              }
          
              // init function that will make fake select. Declared in main JS (atfx-scripts.js)
              select(`[id*=${ formId }]`);
            }
        
            //  look for submitting form event
            if (event.data.type === 'hsFormCallback' && event.data.eventName === 'onFormSubmit') {
              const selectedCountry = $(countrySelect).val();
              // pass selected country to function in order to redirect to website
              hubDbRedirect(selectedCountry, hubdbTable, hubId);
            }
        }
    });
}


function handlePreventSubmit(formId, country, hubdbTable, hubId) {
    axios.get(`https://api.hubapi.com/hubdb/api/v2/tables/${ hubdbTable }/rows?portalId=${ hubId }&country_code=${ country }`)
      .then(function (response) {
      // handle success
      // Always remove warning message and unbind blocking submit button at new request
      $(`[id*=${ formId }] .hs-country_of_residence .countryWarningMessage`).remove();
      $(`[id*=${ formId }] input[type=submit]`).unbind('click');
      
      if (response.data.objects[0].path) {
        //  if website path is true (not null), enable submit button
        $(`[id*=${ formId }] .hs_submit`).removeClass('hs_submit--disabled');
        $(`[id*=${ formId }]`).unbind('submit');
      } else {
        //  if website path is false (null or other falsy value), block button and display warning message
        $(`[id*=${ formId }]`).on('submit', function(e) {
          e.preventDefault();
        });

        $(`[id*=${ formId }] input[type=submit]`).on('click', function(e) {
          e.preventDefault();
        });
        $(`[id*=${ formId }] .hs_submit`).addClass('hs_submit--disabled');
        $(`[id*=${ formId }] .hs-country_of_residence`).append("<div class='countryWarningMessage'><p>Not available for this country</p></div>");
      }
    })
      .catch(function (error) {
      // handle error
      console.log(error);
    })
      .then(function () {
      // always executed
    });
  }


function hubDbRedirect(country, hubdbTable, hubId) {
    axios.get(`https://api.hubapi.com/hubdb/api/v2/tables/${ hubdbTable }/rows?portalId=${ hubId }&country_code=${ country }`)
      .then(function (response) {
      // handle success
      if (response.data.objects[0].path) {
        // Redirect to website declared for country in HubDB 
        window.location = "//" + response.data.objects[0].path;
      }
    })
      .catch(function (error) {
      // handle error
      console.log(error);
    })
      .then(function () {
      // always executed
    });     
  }